<?php

namespace emilasp\social\api\controllers;

use emilasp\course\common\models\Course;
use emilasp\social\common\models\Comment;
use emilasp\social\frontend\models\CommentModel;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;


/**
 * CommentController implements the CRUD actions for CourseUserLink model.
 */
class CommentController extends Controller
{
    public $modelClass = 'emilasp\social\common\models\Comment';

    public function behaviors()
    {
        $behaviors                           = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['only']  = ['list', 'create'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['update'], $actions['create'], $actions['delete'], $actions['view']);
        return $actions;
    }

    /**
     * Список комментариев
     *
     * @param string $object
     * @param int    $id
     * @return ActiveDataProvider
     */
    public function actionList(string $object, int $id)
    {
        $model = $object::findOne($id);

        $query = CommentModel::getBaseQuery($model);

        $query->with(['files', 'createdBy'])
            ->addOrderBy('tree DESC')
            ->addOrderBy('lft')
            ->addOrderBy('created_at');

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    /**
     * Создаём комментарий
     *
     * @return array
     */
    public function actionCreate(): array
    {
        $model = new CommentModel();

        if ($model->load(Yii::$app->request->post(), '')) {
            if ($model->save()) {
                return ['status' => 1, 'data' => []];
            }
        }
        return ['status' => 0, 'data' => $model->model->getErrors()];
    }
}
