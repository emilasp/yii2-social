<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\social\common\models\Rating */

$this->title = Yii::t('social', 'Create Rating');
$this->params['breadcrumbs'][] = ['label' => Yii::t('social', 'Ratings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
