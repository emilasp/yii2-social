<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\social\common\models\Rating */

$this->title = Yii::t('site', 'Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('social', 'Ratings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="rating-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
