<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel emilasp\social\common\models\search\Rating */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('social', 'Ratings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-index">

    <p><?= Html::a(Yii::t('social', 'Create Rating'), ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'object',
            'object_id',
            'status',
            'value',
            // 'ip',
            // 'created_at',
            // 'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
