<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\social\common\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'object')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'object_id')->textInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'ip')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '9[9][9].9[9][9].9[9][9].9[9][9]'
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList($model::$statuses) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('site', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
