<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\social\common\models\Comment */

$this->title = Yii::t('social', 'Create Comment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('social', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
