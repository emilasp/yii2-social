<?php

use emilasp\social\common\models\Comment;
use emilasp\users\common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\social\common\models\search\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('social', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <p><?= Html::a(Yii::t('social', 'Create Comment'), ['create'], ['class' => 'btn btn-success']) ?></p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => '\kartik\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],

            'object',
            'object_id',
            'ip',

            'text:ntext',

            'created_at:datetime',
            [
                'attribute'           => 'created_by',
                'value'               => function ($model) {
                    return $model->createdBy->username ?? null;
                },
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '150px',
                'filterType'          => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'language'      => \Yii::$app->language,
                    'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options'       => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return Comment::$statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => Comment::$statuses,
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
