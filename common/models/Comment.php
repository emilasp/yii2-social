<?php

namespace emilasp\social\common\models;

use creocoder\nestedsets\NestedSetsBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\models\File;
use emilasp\social\common\models\query\CommentQuery;
use emilasp\social\frontend\behaviors\RatingBehavior;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "social_comment".
 *
 * @property int     $id
 * @property string  $object
 * @property int     $object_id
 * @property string  $ip
 * @property string  $name
 * @property string  $email
 * @property string  $text
 * @property int     $status
 *
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 *
 * @property string  $created_at
 * @property string  $updated_at
 * @property int     $created_by
 * @property int     $updated_by
 *
 * @property File[]  $files
 * @property User    $createdBy
 * @property User    $updatedBy
 */
class Comment extends ActiveRecord
{
    public const STATUS_NEW      = 0;
    public const STATUS_APPROVED = 1;
    public const STATUS_SPAM     = 2;
    public const STATUS_DELETED  = 3;

    public static $statuses = [
        self::STATUS_NEW      => 'new',
        self::STATUS_APPROVED => 'approved',
        self::STATUS_SPAM     => 'spam',
        self::STATUS_DELETED  => 'deleted',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'files'              => [
                'class'     => FileBehavior::className(),
                'attribute' => 'files',
            ],
            'rating'             => [
                'class' => RatingBehavior::className(),
            ],
            'nestedSetsBehavior' => [
                'class'         => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * Transaction type
     *
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * Find
     *
     * @return CommentQuery
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object', 'object_id', 'status', 'text', 'name', 'email'], 'required'],
            [['object_id', 'status', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['object_id', 'status', 'created_by', 'updated_by', 'tree', 'lft', 'rgt', 'depth'], 'integer'],
            [['created_at', 'updated_at', 'lft', 'rgt', 'depth'], 'safe'],
            [['text'], 'string'],
            [['object'], 'string', 'max' => 128],
            [['ip', 'name'], 'string', 'max' => 20],
            [['email'], 'email'],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'object'     => Yii::t('social', 'Object'),
            'object_id'  => Yii::t('social', 'Object ID'),
            'ip'         => Yii::t('social', 'User Ip'),
            'name'       => Yii::t('social', 'User Nik'),
            'email'      => Yii::t('social', 'Email'),
            'text'       => Yii::t('social', 'Comment'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * Extra fields
     * @return array
     */
    public function extraFields()
    {
        return [
            'createdBy',
            'files' => function ($model) {
                $files = [];

                foreach ($model->files as $file) {
                    $files[] = ['name' => $file->name, 'url' => $file->getUrl(File::SIZE_ORG)];
                }
                return $files;
            },
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['object_id' => 'id'])
            ->where(['object' => self::class]);
    }

    /**
     * Avatar url
     *
     * @return string
     */
    public function getAvatarUrl(): string
    {
        $avatarUrl = null;

        if (!empty($this->createdBy->profile->image)) {
            $avatarUrl = $this->createdBy->profile->image->getUrl(File::SIZE_ICO);
        } else {
            $hash = md5(strtolower(trim($this->email))); // "MyEmailAddress@example.com"
            $avatarUrl = 'https://www.gravatar.com/avatar/' . $hash;
            //$avatarUrl = File::getNoImageUrl(File::SIZE_ICO);
        }
        return $avatarUrl;
    }

    /** =======================================================================================
     * EVENTS
     * ======================================================================================= */
}
