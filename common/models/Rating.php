<?php

namespace emilasp\social\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "social_rating".
 *
 * @property int    $id
 * @property string $object
 * @property int    $object_id
 * @property int    $status
 * @property int    $value
 * @property string $ip
 * @property string $created_at
 * @property int    $created_by
 *
 * @property User   $createdBy
 */
class Rating extends ActiveRecord
{
    public const STATUS_DELETED  = 0;
    public const STATUS_APPROVED = 1;

    public static $statuses = [
        self::STATUS_APPROVED => 'approved',
        self::STATUS_DELETED  => 'deleted',
    ];

/*    public const TYPE_STARS  = 1;
    public const TYPE_UPDOWN = 2;*/

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [ActiveRecord::EVENT_BEFORE_INSERT => ['created_at']],
                'value'      => new Expression('NOW()'),
            ],
            [
                'class'      => BlameableBehavior::className(),
                'attributes' => [ActiveRecord::EVENT_BEFORE_INSERT => ['created_by']],
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object', 'object_id', 'status', 'value'], 'required'],
            [['object_id', 'status', 'value', 'created_by'], 'default', 'value' => null],
            [['object_id', 'status', 'value', 'created_by'], 'integer'],
            [['created_at'], 'safe'],
            [['object'], 'string', 'max' => 128],
            [['ip'], 'string', 'max' => 20],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'object'     => Yii::t('site', 'Object'),
            'object_id'  => Yii::t('site', 'Object ID'),
            'status'     => Yii::t('site', 'Status'),
            'value'      => Yii::t('social', 'Value'),
            'ip'    => Yii::t('social', 'User Ip'),
            'created_at' => Yii::t('site', 'Created At'),
            'created_by' => Yii::t('site', 'Created By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
