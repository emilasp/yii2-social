<?php

namespace emilasp\social\common\models\query;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use emilasp\core\components\base\BaseActiveQuery;

/**
 * This is the ActiveQuery class for [[Comment]].
 *
 * @see Category
 */
class CommentQuery extends BaseActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}
