<?php
namespace emilasp\social\common;

use emilasp\core\CoreModule;
use emilasp\settings\behaviors\SettingsBehavior;
use emilasp\settings\models\Setting;
use yii\helpers\ArrayHelper;

/**
 * Class SocialBaseModule
 * @package emilasp\social\common
 */
class SocialBaseModule extends CoreModule
{
    /**
     * @return array
     */
    function behaviors()
    {
        return ArrayHelper::merge([
            'setting' => [
                'class'    => SettingsBehavior::className(),
                'meta'     => [
                    'name' => 'Social',
                    'type' => Setting::TYPE_MODULE,
                ],
                'settings' => [
                    [
                        'code'        => 'guest_commentator',
                        'name'        => 'Гостевая отправка комментария',
                        'description' => 'Разрешить оставлять комментарии не авторизованным пользователям.',
                        'default'     => Setting::DEFAULT_SELECT_NO,
                        'type'        => 'select',
                        'data'      => [
                            Setting::DEFAULT_SELECT_NO  => 'Нет',
                            Setting::DEFAULT_SELECT_YES => 'Да',
                        ],
                    ],
                    [
                        'code'        => 'comment_moderation',
                        'name'        => 'Модерация комментариев',
                        'description' => 'Включить модерацию комментариев.',
                        'default'     => Setting::DEFAULT_SELECT_YES,
                        'type'        => 'select',
                        'data'      => [
                            Setting::DEFAULT_SELECT_NO  => 'Нет',
                            Setting::DEFAULT_SELECT_YES => 'Да',
                        ],
                    ],
                ],
            ],
        ], parent::behaviors());
    }
}
