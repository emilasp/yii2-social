<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m170817_071252_add_table_comment
 */
class m170817_071252_add_table_comment extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('social_comment', [
            'id'        => $this->primaryKey(11),
            'object'    => $this->string(128)->notNull(),
            'object_id' => $this->integer(11)->notNull(),
            'status'    => $this->smallInteger(1)->notNull(),
            'ip'        => $this->string(20)->notNull(),
            'name'      => $this->string(20)->notNull(),
            'email'     => $this->string(150)->notNull(),
            'text'      => $this->text()->notNull(),

            'tree'  => $this->integer(),
            'lft'   => $this->integer()->notNull(),
            'rgt'   => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),

            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_social_comment_created_by',
            'social_comment',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_social_comment_updated_by',
            'social_comment',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('social_comment_model', 'social_comment', ['object', 'object_id', 'status']);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('social_comment');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
