<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170817_081256_add_table_rating extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('social_rating', [
            'id'         => $this->primaryKey(11),
            'object'     => $this->string(128)->notNull(),
            'object_id'  => $this->integer(11)->notNull(),
            'status'     => $this->smallInteger(1)->notNull(),
            'value'      => $this->smallInteger(1)->notNull(),
            'ip'         => $this->string(20),
            'created_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_social_rating_created_by',
            'social_rating',
            'created_by',
            'users_user',
            'id'
        );

        $this->createIndex('social_rating_model', 'social_rating', ['object', 'object_id', 'status', 'ip']);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('social_rating');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
