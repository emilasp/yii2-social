<?php

namespace emilasp\social\frontend\controllers;

use emilasp\core\components\base\Controller;
use emilasp\social\common\models\search\RatingSearch;
use Yii;
use emilasp\social\common\models\Rating;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RatingController implements the CRUD actions for Rating model.
 */
class RatingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'rate' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rating models.
     * @return mixed
     */
    public function actionRate()
    {
        $value    = Yii::$app->request->post('value');
        $object   = Yii::$app->request->post('object');
        $objectId = Yii::$app->request->post('objectId');

        if ($model = $object::findOne($objectId)) {
            if ($model->setRating((float)$value)) {
                return $this->setAjaxResponse(
                    1,
                    Yii::t('social', 'Rating update'),
                    ['value' => $model->getRating()]
                );
            }
        }

        return $this->setAjaxResponse(0, Yii::t('social', 'Rating no update!'));
    }
}
