<?php

namespace emilasp\social\frontend\behaviors;

use emilasp\social\common\models\Rating;
use yii;
use yii\base\Behavior;

/**
 * Поведение - добавляем функционал работы с рейтингами
 *
 * Class RatingBehavior
 * @package emilasp\social\frontend\behaviors
 */
class RatingBehavior extends Behavior
{
    /**
     * Получаем количество комментариев для моделиs
     *
     * @return int
     */
    public function getRatingCount(): int
    {
        $sqlRating = <<<SQL
        SELECT count(*) as rating FROM social_rating
        WHERE object='{$this->owner::className()}' AND object_id='{$this->owner->id}';
SQL;
        return (int)Yii::$app->db->createCommand($sqlRating)->queryScalar();
    }

    /**
     * Получаем количество комментариев для моделиs
     *
     * @return float
     */
    public function getRating(): float
    {
        $sqlRating = <<<SQL
        SELECT AVG(value) as rating FROM social_rating
        WHERE object='{$this->owner::className()}' AND object_id='{$this->owner->id}';
SQL;
        return (float)Yii::$app->db->createCommand($sqlRating)->queryScalar();
    }

    /**
     * Добавляем новый рейт
     *
     * @param float $rate
     * @return bool
     */
    public function setRating(float $rate): bool
    {
        /*if (!$this->check()) {
            return false;
        }*/

        $params = [
            'object'    => $this->owner::className(),
            'object_id' => $this->owner->id,
            'ip'   => Yii::$app->request->userIP
        ];

        if (!$rating = Rating::findOne($params)) {
            $rating         = new Rating($params);
            $rating->status = Rating::STATUS_ENABLED;
        }

        $rating->value = (int) $rate;

        return $rating->save();
    }

    /**
     * Проверяем, что пользовтель ещё не голосовал
     *
     * @return bool
     */
    private function check(): bool
    {
        $userIp    = Yii::$app->request->userIP;
        $sqlRating = <<<SQL
        SELECT count(*) FROM social_rating
        WHERE object='{$this->owner::className()}' AND object_id='{$this->owner->id}' AND ip='{$userIp}';
SQL;
        return !(bool)Yii::$app->db->createCommand($sqlRating)->queryScalar();
    }
}
