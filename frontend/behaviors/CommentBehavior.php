<?php
namespace emilasp\social\frontend\behaviors;

use emilasp\social\frontend\models\CommentModel;
use yii;
use yii\base\Behavior;

/**
 * Поведение добавляем функционал работы с комментариями
 *
 * Class CommentBehavior
 * @package emilasp\social\frontend\behaviors
 */
class CommentBehavior extends Behavior
{
    /**
     * Получаем количество комментариев для моделиs
     *
     * @return int
     */
    public function getCommentCount(): int
    {
        $query = CommentModel::getBaseQuery($this->owner);
        return $query->count();
    }
}
