$(document).on('click', '.btn-remove-reply', function () {
    setReply('', '');
});

$(document).on('click', '.btn-to-comment-form', function () {
    $('html, body').animate({
        scrollTop: $("#comment-form-id").offset().top
    }, 1000);
});

$(document).on('click', '.btn-comment-reply', function () {
    var id = $(this).data('id');
    var replyText = $(this).closest('.comment-item').find('.comment-text').html();

    setReply(id, replyText);

    $('html, body').animate({
        scrollTop: $("#comment-form-id").offset().top
    }, 1000);
});

function setReply(id, replyText)
{
    if (id) {
        $('#comment_parent_id').val(id);
        $('.comment-form-meta').html('<strong>Reply:</strong> ' + replyText);
        $('.btn-remove-reply').show();
    } else {
        $('#comment_parent_id').val('');
        $('.comment-form-meta').text('');
        $('.btn-remove-reply').hide();
    }
}