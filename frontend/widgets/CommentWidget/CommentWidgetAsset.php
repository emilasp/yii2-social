<?php

namespace emilasp\social\frontend\widgets\CommentWidget;

use emilasp\core\components\base\AssetBundle;
use emilasp\core\components\base\View;

/**
 * Class ImCart
 * @package emilasp\social\frontend\widgets\CommentWidget
 */
class CommentWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    //public $jsOptions = ['position' => View::POS_READY];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $css = [
        'comment'
    ];
    public $js = [
        'comment'
    ];
}
