<?php

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php if (Yii::$app->user->isGuest && !Yii::$app->getModule('social')->getSetting('guest_commentator')) : ?>

    <div class="card">
        <div class="card-header">
            <?= Yii::t('social', 'Comment form title') ?>
        </div>
        <div class="card-body">
            <a href="#" class="auth-form">
                <h5 class="card-title">
                    <?= Yii::t('social', 'Registered for send new comment') ?>
                </h5>
            </a>
        </div>
    </div>

<?php else: ?>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title"><?= Yii::t('social', 'Comment form title') ?></h5>
            <div class="clearfix">
                <div class="comment-form-meta text-muted float-md-right"></div>
                <div class="float-md-right">
                    <button class="btn btn-xs btn-danger btn-remove-reply" style="display: none">
                        <i class="fa-cancel"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">

            <?php
            $form = ActiveForm::begin([
                'id'      => 'comment-form-id',
                'options' => ['name' => 'user-form-ajax', 'data-pjax' => 1]
            ]);
            ?>

            <?= Html::activeHiddenInput($model, 'object'); ?>
            <?= Html::activeHiddenInput($model, 'object_id'); ?>
            <?= Html::activeHiddenInput($model, 'parent_id', ['id' => 'comment_parent_id']); ?>

            <?php if (Yii::$app->user->isGuest && Yii::$app->getModule('social')->getSetting('guest_commentator')) : ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'name')->textInput([
                            'placeholder' => Yii::t('social', 'User nik'),
                            'disabled'    => (bool)$model->name
                        ])->label(false) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput([
                            'placeholder' => Yii::t('social', 'Email'),
                            'disabled'    => (bool)$model->email
                        ])->label(false) ?>
                    </div>
                </div>

            <?php else : ?>
                <?= Html::activeHiddenInput($model, 'name') ?>
                <?= Html::activeHiddenInput($model, 'email') ?>
            <?php endif; ?>


            <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                'options'       => ['rows' => 8],
                'preset'        => 'custom',
                'clientOptions' => [
                    'filebrowserUploadUrl' => '/media/file/cke-upload',
                    'toolbarGroups'        => [
                        ['name' => 'undo'],
                        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                        ['name' => 'colors'],
                        ['name' => 'links', 'groups' => ['links', 'insert']],
                        ['name' => 'others', 'groups' => ['others', 'about']],
                    ],
                    'removeButtons'        => 'Subscript,Superscript,Flash,Table,Image,SpecialChar,PageBreak,Iframe,About',
                    'removePlugins'        => 'elementspath',
                ]
            ]) ?>

            <div class="form-actions text-right">
                <?= Html::submitButton(Yii::t('social', 'Add comment'), ['class' => 'btn btn-primary']); ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

<?php endif; ?>
