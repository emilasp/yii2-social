<?php

use yii\widgets\ListView;


/* @var $this yii\web\View */
/* @var $model emilasp\social\common\models\Comment */
?>

<div class="comment-conteiner">

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        /*'pager'        => [
            'firstPageLabel'    => Icon::show('angle-double-left', ['class' => 'fa-1x'], \kartik\icons\Icon::FA),
            'lastPageLabel'     => Icon::show('angle-double-right', ['class' => 'fa-1x'], \kartik\icons\Icon::FA),
            'nextPageLabel'     => Icon::show('angle-right', ['class' => 'fa-1x'], \kartik\icons\Icon::FA),
            'prevPageLabel'     => Icon::show('angle-left', ['class' => 'fa-1x'], \kartik\icons\Icon::FA),
        ],*/
        'itemOptions'  => ['class' => 'item'],
        'itemView'     => function ($model, $key, $index, $widget) use ($owner) {
            return $this->render('parts/_item', ['model' => $model, 'owner' => $owner]);
        },
        'summary'=>'',
        'options'      => ['id' => 'comment-tree-list-id', 'class' => 'list-view']
    ]);
    ?>

</div>