<?php
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model emilasp\social\common\models\Comment */
?>

<?php Pjax::begin([
    'id'              => 'comment-tree-id',
    'enablePushState' => false,
    'options'         => [
        'class' => 'no-loading'
    ]
]); ?>

<div class="social-comment-container">
    <?php if (!Yii::$app->user->isGuest || Yii::$app->getModule('social')->getSetting('guest_commentator')) : ?>
        <button class="btn btn-primary btn-to-comment-form"><?= Yii::t('social', 'Send comment') ?></button>
    <?php endif; ?>

    <?= $this->render('list', ['dataProvider' => $dataProvider, 'owner' => $model]) ?>
    <hr />
    <?= $this->render('form', ['model' => $modelComment]) ?>
</div>

<?php Pjax::end(); ?>


