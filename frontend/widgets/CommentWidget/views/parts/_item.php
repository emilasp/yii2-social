<?php

use emilasp\media\models\File;
use emilasp\social\frontend\widgets\RatingWidget\RatingWidget;
use yii\helpers\Html;

?>


<div class="comment-item well well-sm media p-3" style="margin-left: <?= $model->depth * 20 ?>px">

    <?php if ($model->depth) : ?>
        <div class="comment-subcomment">
            <i class="fa fa-angle-double-right"></i>
        </div>
    <?php endif; ?>

    <?= Html::img($model->getAvatarUrl(), [
        'class' => 'comment-avatar media-object mr-3 mt-3 rounded-circle',
    ]) ?>

    <div class="media-body">
        <div class="row commentBlockTop">
            <div class="col-md-4 text-primary">
                <i class="fa fa-user"></i>
                <?= $model->name ?: Yii::t('site', 'Guest') ?>
            </div>
            <div class="col-md-8 text-right">
                <div class="float-md-right">
                    <?= RatingWidget::widget(['model' => $model, 'type' => RatingWidget::TYPE_UPDOWN]) ?>
                </div>
                <div class="float-md-right text-meta">
                    <i class="fa fa-clock"></i>
                    <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 comment-text"><?= $model->text ?></div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right">
                <button class="btn btn-xs btn-success btn-comment-reply" data-id="<?= $model->id ?>">
                    <?= Yii::t('social', 'Reply') ?>
                </button>
            </div>
        </div>
    </div>

</div>
