<?php

namespace emilasp\social\frontend\widgets\CommentWidget;

use emilasp\core\components\base\ActiveRecord;
use emilasp\core\components\base\Widget;
use emilasp\social\frontend\models\CommentModel;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CommentWidget
 * @package emilasp\social\frontend\widgets\CommentWidget
 */
class CommentWidget extends Widget
{
    /** @var  ActiveRecord */
    public $model;

    /**
     * INIT
     */
    public function init(): void
    {
        $this->registerAssets();
    }

    /**
     * RUN
     */
    public function run(): void
    {
        echo $this->render('index', [
            'model'        => $this->model,
            'modelComment' => $this->getModel(),
            'dataProvider' => $this->getDataProvider(),
        ]);
    }

    /**
     * Получаем модель комментария
     *
     * @return CommentModel
     */
    private function getModel(): CommentModel
    {
        $model = new CommentModel([
            'object'    => $this->model::className(),
            'object_id' => $this->model->id,
        ]);

        if (!Yii::$app->user->isGuest) {
            $model->name  = Yii::$app->user->identity->profile->fullName;
            $model->email = Yii::$app->user->identity->email;
        } else {
            $model->name  = Yii::$app->session->get('comment:fullName');
            $model->email = Yii::$app->session->get('comment:email');
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (Yii::$app->user->isGuest && !Yii::$app->session->get('comment:email')) {
                    Yii::$app->session->set('comment:fullName', $model->name);
                    Yii::$app->session->set('comment:email', $model->email);
                }

                $model->setAttributes([
                    'text'      => '',
                    'parent_id' => '',
                ]);
            }
        }

        return $model;
    }

    /**
     * Формируем dataprovider
     *
     * @return ActiveDataProvider
     */
    private function getDataProvider(): ActiveDataProvider
    {
        $query = CommentModel::getBaseQuery($this->model);

        $query->with('createdBy')
            ->addOrderBy('tree ASC')
            ->addOrderBy('lft')
            ->addOrderBy('created_at');

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['pageSize' => 1000],
        ]);
    }

    /**
     * Register client assets
     */
    private function registerAssets(): void
    {
        CommentWidgetAsset::register($this->view);
    }
}
