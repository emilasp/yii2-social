<?php
use yii\helpers\Url;
?>

    <div class="clearfix rating-updown text-right" data-object="<?= $model::className() ?>" data-object-id="<?= $model->id ?>">

        <span class="rate-up" data-value="1"> <i class="fa fa-thumbs-up "></i></span>
        <span class="rate-value text-<?= $model->getRating() >= 0 ? 'success' : 'danger' ?>">
            <?= $model->getRating() ?>
        </span>
        <span class="rate-down" data-value="-1"> <i class="fa fa-thumbs-down "></i></span>

    </div>

