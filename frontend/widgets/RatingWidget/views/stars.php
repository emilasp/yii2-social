<?php

use kartik\widgets\StarRating;
use yii\helpers\Url;
use \yii\widgets\Pjax;

$ratingId = 'rating' . $model->id;
?>

    <div class="rating-container-div clearfix" id="<?= $ratingId ?>" data-object="<?= $model::className() ?>"
         data-object-id="<?= $model->id ?>">

        <div class="float-md-left">

            <?php if ($rate = $model->getRating()) : ?>
                <div class="meta-item" itemscope itemtype="http://schema.org/AggregateRating"
                     itemprop="aggregateRating">
                    <div itemprop="ratingValue"><?= $rate ?></div>
                    <div itemprop="bestRating"><?= $maxRate ?></div>
                    <div itemprop="worstRating">1</div>
                    <div itemprop="ratingCount"><?= $model->getRatingCount() ?></div>
                </div>
            <?php endif; ?>

            <?= StarRating::widget([
                'name'          => 'rating_stars',
                'value'         => (int)$rate,
                'language'      => 'ru',
                'pluginOptions' => [
                    'theme'              => 'krajee-uni',
                    'filledStar'         => '&#x2605;',
                    'emptyStar'          => '&#x2606;',
                    'showClear'          => false,
                    'showCaption'        => false,
                    'step'               => 1,
                    //'min'  => 1,
                    'max'                => $maxRate,
                    'size'               => 'custom',
                    'starCaptions'       => [
                        1 => '1',
                        2 => '2',
                        3 => '3',
                        4 => '4',
                        5 => '5',
                    ],
                    'starCaptionClasses' => [
                        1 => 'text-danger',
                        2 => 'text-warning',
                        3 => 'text-info',
                        4 => 'text-primary',
                        5 => 'text-success',
                    ],
                ],
                'pluginEvents'  => [
                    'rating:change' => "function(event, value, caption) { updateRating{$ratingId}(value) }"
                ]
            ]) ?>
        </div>

        <?php Pjax::begin(['id' => 'rate-star-counter', 'options' => ['class' => 'float-left']]); ?>

        <div class="badge badge-success float-md-left">
            <i class="fa fa-star"></i> <?= $model->getRating() ?>
        </div>
        <?php Pjax::end(); ?>

    </div>

<?php
$urlRate = Url::toRoute(['/social/rating/rate']);

$js = <<<JS
function updateRating{$ratingId}(value) {
    var input = $('#{$ratingId}');
    var object = input.data('object');
    var objectId = input.data('object-id');
    
    console.log("Test rate");
    
    $.ajax({
        type: 'POST',
        url: '{$urlRate}',
        dataType: "json",
        data: 'object=' + object + '&objectId='+objectId + '&value='+value,
        success: function(msg) {
            if (msg['status']=='1') {
                input.find('.label').text(msg['data']['value']);
            }
            notice(msg['message'], (msg['status']=='1' ? 'green' : 'red'));
            
             $.pjax({
                 container:"#rate-star-counter",
                 timeout: 0,
                 push:false,
                 scrollTo:false
             });
        },
        error: function(){}
    });

}
JS;

$this->registerJs($js);

