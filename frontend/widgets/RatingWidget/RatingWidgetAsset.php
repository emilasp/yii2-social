<?php

namespace emilasp\social\frontend\widgets\RatingWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class RatingWidget
 * @package emilasp\social\frontend\widgets\RatingWidget
 */
class RatingWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $css = [
        'rating'
    ];
    public $js = [
        'rating'
    ];
}
