$(document).on('click', '.rate-up, .rate-down', function() {
    var element = $(this);
    var valueEl = element.closest('.rating-updown').find('.rate-value');
    var value = element.data('value');
    var parent = element.closest('.rating-updown');
    var object = parent.data('object');
    var objectId = parent.data('object-id');

    $.ajax({
        type: 'POST',
        url: '/social/rating/rate.html',
        dataType: "json",
        data: 'object=' + object + '&objectId='+objectId + '&value='+value,
        success: function(msg) {
            if (msg['status']=='1') {
                parent.find('.badge').text(msg['data']['value']);

                var value = parseInt(msg['data']['value']);

                if (value > 0) {
                    valueEl.removeClass('text-danger').addClass('text-success');
                } else if (value < 0) {
                    valueEl.removeClass('text-success').addClass('text-danger');
                } else if (value === 0) {
                    valueEl.removeClass('text-success').removeClass('text-danger').addClass('text-default');
                }
                valueEl.html(value);
            }
            notice(msg['message'], (msg['status']=='1' ? 'green' : 'red'));
        }
    });
});