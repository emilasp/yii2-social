<?php

namespace emilasp\social\frontend\widgets\RatingWidget;

use emilasp\core\components\base\ActiveRecord;
use emilasp\core\components\base\Widget;
use emilasp\social\frontend\models\RatingModel;
use Yii;

/**
 * Class RatingWidget
 * @package emilasp\social\frontend\widgets\RatingWidget
 */
class RatingWidget extends Widget
{
    public const TYPE_STARS  = 'stars';
    public const TYPE_UPDOWN = 'updown';

    /** @var  ActiveRecord */
    public $model;

    /** @var int Тип рейтинга - звёзды, голосование */
    public $type = self::TYPE_STARS;

    /** @var int Максимальное значение - оно же говорит сколько будет звёзд */
    public $maxRate = 5;

    /**
     * INIT
     */
    public function init(): void
    {
        $this->registerAssets();
    }

    /**
     * RUN
     */
    public function run(): void
    {
        echo $this->render($this->type, [
            'model'       => $this->model,
            'maxRate'     => $this->maxRate
        ]);
    }

    /**
     * Register client assets
     */
    private function registerAssets(): void
    {
        RatingWidgetAsset::register($this->view);
    }
}
