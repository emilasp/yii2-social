<?php

namespace emilasp\social\frontend\models;

use emilasp\social\common\models\Comment;
use Yii;
use yii\base\Model;
use yii\caching\DbDependency;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\UploadedFile;

/**
 *
 * Class CommentModel
 *
 * @property string $object
 * @property int    $object_id
 * @property string $name
 * @property string $email
 * @property string $text
 *
 */
class CommentModel extends Model
{
    public $object;
    public $object_id;
    public $parent_id;
    public $name;
    public $email;
    public $text;

    public $model;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object', 'object_id', 'text'], 'required'],
            [
                ['email', 'name'],
                'required',
                'when' => function ($model) {
                    return Yii::$app->user->isGuest;
                }
            ],

            [['object_id', 'parent_id'], 'integer'],
            [['text'], 'string'],
            [['object'], 'string', 'max' => 128],
            [['email'], 'email'],
            [['name'], 'string', 'max' => 20],
        ];
    }

    /**
     * Save comment
     *
     * @return bool
     */
    public function save(): bool
    {
        $this->model = new Comment([
            'object'    => $this->object,
            'object_id' => $this->object_id,
            'name'      => $this->name,
            'email'     => $this->email,
            'text'      => $this->text,
            'status'    => Comment::STATUS_NEW,
            'ip'        => Yii::$app->request->userIP,
        ]);


        if ($this->parent_id && $parent = Comment::findOne($this->parent_id)) {
            $isSave = $this->model->appendTo($parent);
        } else {
            $isSave = $this->model->makeRoot();
        }

        if ($isSave) {
            $files = UploadedFile::getInstancesByName('file');

            foreach ($files as $index => $file) {
                if ($file) {
                    $this->model->saveImage($this->model->id, $file);
                }
            }
        }


        return $isSave;
    }

    /**
     * Формируем Query дял фронта
     *
     * @return Query
     */
    public static function getBaseQuery(ActiveRecord $model): Query
    {
        return Yii::$app->db->cache(function ($db) use ($model) {
            $statuses = [Comment::STATUS_APPROVED];

            if (!Yii::$app->getModule('social')->getSetting('comment_moderation')) {
                $statuses[] = Comment::STATUS_NEW;
            }

            return Comment::find()->where([
                'object'    => $model::className(),
                'object_id' => $model->id,
                'status'    => $statuses,
            ])->orWhere([
                'object'    => $model::className(),
                'object_id' => $model->id,
                'status'    => Comment::STATUS_NEW,
                'ip'        => Yii::$app->request->userIP
            ])->orWhere([
                'object'     => $model::className(),
                'object_id'  => $model->id,
                'status'     => Comment::STATUS_NEW,
                'created_by' => (int)Yii::$app->user->id,
            ]);
        }, null, new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM social_comment']));
    }
}
